class AddSalaryAndRateToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :salary, :decimal, precision: 10, scale: 2
    add_column :admin_users, :rate, :decimal, precision: 10, scale: 2
  end
end
