class AddAdditionalNameToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :additional_name, :string
  end
end
