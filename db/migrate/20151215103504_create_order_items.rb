class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :order_id
      t.integer :event_id
      t.integer :quant
      t.integer :promocode_id
      t.integer :activated, default: 0
      t.timestamps null: false
    end
  end
end
