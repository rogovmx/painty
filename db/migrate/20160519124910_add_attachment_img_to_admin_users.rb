class AddAttachmentImgToAdminUsers < ActiveRecord::Migration
  def self.up
    change_table :admin_users do |t|
      t.attachment :img
    end
  end

  def self.down
    remove_attachment :admin_users, :img
  end
end
