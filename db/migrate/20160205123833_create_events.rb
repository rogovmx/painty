class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.text :descr
      t.datetime :start_date
      t.datetime :end_date
      t.attachment :image
      t.integer :admin_user_id
      t.integer :max_guests
      t.boolean :visible
      t.boolean :discount, default: true
      t.string :admin_link
      t.integer :category_id
      t.integer :address_id
      t.integer :remain
      t.decimal :price, precision: 999, scale: 2

      t.timestamps null: false
    end
  end
end
