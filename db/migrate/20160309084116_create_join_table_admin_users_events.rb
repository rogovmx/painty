class CreateJoinTableAdminUsersEvents < ActiveRecord::Migration
  def change
    create_join_table :admin_users, :events do |t|
      t.index [:admin_user_id, :event_id]
      t.index [:event_id, :admin_user_id]
    end
  end
end
