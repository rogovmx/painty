class RemoveRemainFromEvent < ActiveRecord::Migration
  def change
    remove_column :events, :remain, :integer
  end
end
