class CreateCertTypes < ActiveRecord::Migration
  def change
    create_table :cert_types do |t|
      t.string :name
      t.boolean :display
      t.integer :category_id
      t.integer :price
      t.attachment :img
      t.timestamps null: false
    end
  end
end
