class RolifyCreateRoles < ActiveRecord::Migration
  def change
    create_table(:roles) do |t|
      t.string :name
      t.references :resource, :polymorphic => true

      t.timestamps
    end unless table_exists?("roles")

    create_table(:admin_users_roles, :id => false) do |t|
      t.references :admin_user
      t.references :role
    end unless table_exists?("admin_users_roles")

    add_index(:roles, :name) unless table_exists?("roles")
    add_index(:roles, [ :name, :resource_type, :resource_id ]) unless table_exists?("roles")
    add_index(:admin_users_roles, [ :admin_user_id, :role_id ]) unless table_exists?("admin_users_roles")
  end
end
