class AddCertificateIdToCart < ActiveRecord::Migration
  def change
    add_column :carts, :certificate_id, :integer
  end
end
