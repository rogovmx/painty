class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.integer :cart_id
      t.integer :event_id
      t.integer :quant

      t.timestamps null: false
    end
  end
end
