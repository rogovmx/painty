class AddPromocodeIdToCart < ActiveRecord::Migration
  def change
    add_column :carts, :promocode_id, :integer
  end
end
