class AddCertTypeIdToCertificate < ActiveRecord::Migration
  def change
    add_column :certificates, :cert_type_id, :integer
  end
end
