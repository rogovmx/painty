class AddColumnsToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :url, :string
    add_column :addresses, :name, :string
  end
end
