class CreateEventMailStatuses < ActiveRecord::Migration
  def change
    create_table :event_mail_statuses do |t|
      t.references :event, foreign_key: true
      t.references :client, foreign_key: true

      t.timestamps null: false
    end
    add_index :event_mail_statuses, [:event_id, :client_id], unique: true
  end
end
