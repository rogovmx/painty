class AddCertificateIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :certificate_id, :integer
  end
end
