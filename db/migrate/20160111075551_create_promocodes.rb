class CreatePromocodes < ActiveRecord::Migration
  def change
    create_table :promocodes do |t|
      t.string :name
      t.string :discount
      t.datetime :start_date
      t.datetime :end_date
      t.boolean :active
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
