class CreateCertificates < ActiveRecord::Migration
  def change
    create_table :certificates do |t|
      t.string :code
      t.integer :discount
      t.string :name
      t.boolean :used
      t.boolean :paid
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
