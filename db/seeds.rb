# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
adm = 
  AdminUser.find_or_create_by!(email: 'admin@example.com') do |user|
    user.name = "Админ"
    user.salary = 1000
    user.rate = 1
    user.password = 'password'
    user.password_confirmation = 'password'
  end
radmin = Role.find_or_create_by!(name: "admin")
Role.find_or_create_by!(name: "Босс")
Role.find_or_create_by!(name: "Менеджер")
Role.find_or_create_by!(name: "Художник")
Role.find_or_create_by!(name: "Фотограф")

adm.roles << radmin
adm.save
