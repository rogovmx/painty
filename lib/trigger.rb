class Trigger
  require 'mandrill'

  FROM_NAME = 'Painty'
  FROM_EMAIL = 'help@painty.ru'


  def self.api
    Mandrill::API.new '4DRANavnxB0rYnB3Qv3FDA'
    # Mandrill::API.new 'AqQ_--qbhImJDhd9orb3OA'
  end


  # Ваш подарочный сертификат
  # slug: developer-2
  # @param [Certificate] certificate
  def self.send_certificate certificate
    subject = 'Сертификат на Painty'
    template_name = 'developer-2'
    template_content = nil

    message = {
        :global_merge_vars => [
            {
                :name => 'seats',
                :content => '2 ' + Russian.p(2, 'место', 'места', 'мест')
            },
            {
                :name => 'active_to',
                # TODO: заменить на дату окончания действия сертификата
                :content => '17 августа 2016 г.'
            },
            {
                :name => 'number',
                :content => certificate.code
            }
        ],
        :to => [
            {
                :type => 'to',
                :email => certificate.client.email
            }
        ],
        :from_name => FROM_NAME,
        :from_email => FROM_EMAIL,
        :subject => subject,
    }

    async = false
    ip_pool = nil
    send_at = nil
    result = api.messages.send_template template_name, template_content, message, async, ip_pool, send_at
    Rails.logger.debug "debug: #{result}"
  end

  # Ваш вечер с Painty
  # slug: develop-1
  # @param [Event] event
  def self.send_event event, client
    template_name = 'develop-1'
    template_content = nil
    subject = 'Ваш вечер с Painty'

    event_user_img = ''
    event_user_text = ''
    for painter in event.admin_users do
      if painter.painter?
        event_user_img = 'http://painty.ru/' + painter.img.url(:w100)
        event_user_text = "До яркой встречи, #{painter.name}!"
      end
    end
    if event.address
      event_address = event.address.name.to_s + ' на ' + event.address.address.to_s
    else
      event_address = ''
    end

    message = {
        :global_merge_vars => [
            {
                :name => 'name_content',
                :content => client.name + ', спасибо, что провели вечер с Painty!'
            },
            {
                :name => 'img',
                :content => 'http://painty.ru/' + event.image.url(:w200)
            },
            {
                :name => 'event_date',
                :content => event.email_date
            },
            {
                :name => 'event_name',
                :content => event.title
            },
            {
                :name => 'event_address',
                :content => event_address
            },
            {
                :name => 'event_user_img',
                :content => event_user_img
            },
            {
                :name => 'event_user_text',
                :content => event_user_text
            }
        ],
        :to => [
            {
                :type => 'to',
                :email => client.email
            }
        ],
        :from_name => FROM_NAME,
        :from_email => FROM_EMAIL,
        :subject => subject,
    }
    Rails.logger.debug "debug #{message}"
    async = false
    ip_pool = nil
    send_at = nil

    # [{'status'=>'sent',
    #     'reject_reason'=>'hard-bounce',
    #     'email'=>'recipient.email@example.com',
    #     '_id'=>'abc123abc123abc123abc123abc123'}]
    result = api.messages.send_template template_name, template_content, message, async, ip_pool, send_at
    Rails.logger.debug "debug: #{result}"
  end


  # @param [Order] order
  def self.send_order(order)
    @order = order

    @client = @order.client
    @subject = 'Ваши места на Painty'

    template = Tilt::ERBTemplate.new "#{Rails.root}/app/templates/order.html.erb"
    output = template.render self
    message = {
        :html => output,
        :to => [
            {
                :type => 'to',
                :email => @client.email
            }
        ],
        :from_name => FROM_NAME,
        :from_email => FROM_EMAIL,
        :subject => @subject,
    }
    async = false
    ip_pool = nil
    send_at = nil
    result = api.messages.send message, async, ip_pool, send_at
    Rails.logger.debug "debug: #{result}"
  end

  # Выбрать события которые закончились
  # есть в заказах
  # вместе с клиентами
  # и без записей в :event_mail_statuses
  #
  # тогда отправляем
  # self.send_event event, client
  # и делаем записть в базу об отправке
  #
  def self.events
    events = Event.all
                 .joins(:orders)
    # .where('end_date < ?', Time.now.midnight)
    client = Client.last
    events.each do |event|
      Trigger.send_event event, client
      # EventMailStatus.create :event => event, :client => client
    end
  end
end