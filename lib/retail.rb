class Retail
  require 'retailcrm'
 
  def self.api
#    key = Rails.env.production? ? '' : ''   
    key = 'REOVGwzKv6MkZai5C1ooNHtELCXDdZt0'   
    Retailcrm.new('https://painty.retailcrm.ru', key) 
  end
  
  
  # Создание или обновления пользователя retailcrm со склейкой по email
  def self.create_or_update_customer client
    customer = get_customer(client)
    if customer
      api.customers_edit(customer_data client)
    else  
      api.customers_create(customer_data client)
    end 
  end
  
  # Поиск пользователя retailcrm по почте (пока вытаскиваются первые 100 )
  def self.find_customer email
    resp = api.customers({email: email}, 100, 1).response
    if resp['success']
      resp['customers'].first['externalId']
    else
      false
    end
  end
  
  
  # Создание пользователя retailcrm
  def self.create_customer client
    api.customers_create(customer_data client)
  end
  

  
  # Создание или редактирование заказа retailcrm
  def self.create_or_update_order order
    customer = get_customer(order.client)
    create_customer(order.client) unless customer #создаем пользователя если его нет в retailcrm
    if get_order order
      api.orders_edit(order_data order).response
    else
      api.orders_create(order_data order).response
    end
  end
  
  # Поиск заказа retailcrm по externalId (id в базе магазина)
  def self.get_order order
    getorder = api.orders_get(order.id).response['order']
    getorder
  end
  
  # Поиск пользователя retailcrm по externalId (id в базе магазина)
  def self.get_customer client
    api.customers_get(client.id).response['customer']
  end
  
  
  def self.get_goods
    api.store_inventories
  end
  
  # Данные пользователя из базы сайта
  def self.customer_data client
    { 
      externalId: client.id,
      firstName: client.name,
      lastName: client.last_name,
      email: client.email,
      phones: [{number: client.phone}]
    }
end
  
  
  # Данные заказа
  def self.order_data order
    {
      customerId: order.client.id,
      externalId: order.id,
      number: order.id.to_s,
      createdAt: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
      email: order.client ? order.client.email : '',
      firstName: order.client ? order.client.name : '',
      lastName: order.client ? order.client.last_name : '',
      phone: order.client ? order.client.phone : '',
      paymentType: "payture",
      shipmentStore: 'painty',
      paymentStatus: pay_status(order),
      discountPercent: order.percent_by_quant,
      customer: order_customer(order),
      status: order.status,
      items: order_items(order),
      customFields: {promo: order.promocode ? order.promocode.name + " (#{order.promocode.discount}%)" : "" }
    }

  end
  
  # Заказанные позиции
  def self.order_items order
    if order.certificate
      cert = [{
        productId: "ct#{order.certificate.cert_type.id}", 
        initialPrice: order.certificate.cert_type.price, 
        quantity: order.certificate.quant,
        comment: "Сертификат"
      }]
    end 
    evts = 
      order.order_items.map do |order_item| 
        {
          productId: order_item.event.id, 
          initialPrice: order_item.event.price, 
          quantity: order_item.quant,
          discountPercent: order_item.promo_percent,
          comment: "Использовано билетов: #{order_item.activated}"
        } 
      end
    cert ? evts + cert : evts
  end
  
  # Пользователь создающий заказ
  def self.order_customer order
    {externalId: order.client.id,
    firstName:  order.client.name,
    lastName:  order.client.last_name,
    phones: [{ number: order.client.phone }]} 
  end
  
  
  # Статус оплаты
  def self.pay_status order
    case order.status
    when 1
      "paid"
    else
      "not-paid"
    end
  end
  
end