$(document).ready(function () {  
  
  var host = "http://painty.rogov.lclients.ru/"
//  var host = "http://localhost:3001/"
  console.log(host);
  
  function hum_quant(q) {
    if (q === 1) { h = q + " место ";};
    if (q > 1 && q < 5 ) { h = q + " места ";};
    if (q > 4 ) { h = q + " мест ";};
    return h;
  }

  
  function set_cart(event, quant) {
    $.ajax({
        type: 'POST',
        url: host + "events/edit_cart",
        data: {
            event: event,
            quant: quant,
            p_cart: $.cookie("p_cart"),
            city: $.cookie('city_tickets')
        },
        dataType: 'json',
        success: function (data) {
          $.cookie("p_cart", data.p_cart);
          $("span.discount").text(data.discount);
          $.each(data.evts, function (index, value) {
            $('[data-event-id="' + value[0] + '"]').find('span.price').text(value[1] + "р. ");
          });
        }
    });
  }
  
  function set_cart_cert(cert, quant) {
    $.ajax({
        type: 'POST',
        url: host + "events/edit_cart_cert",
        data: {
            cert: cert,
            quant: quant,
            p_cart: $.cookie("p_cart"),
            city: $.cookie('city_tickets')
        },
        dataType: 'json',
        success: function (data) {
          $.cookie("p_cart", data.p_cart);
          $("span.discount").text(data.discount);
          $("#cert-block span.price").text(data.cost + " р. ");
        }
    });
  }
   
  function get_events(city) {
    in_city = city || $.cookie('city_tickets');
    $.ajax({
      type: "GET",
      url: host + "events?p_cart=" + $.cookie("p_cart") + "&city=" + in_city,
      dataType: "script"
    });
  };
  
  	// Close overlay with purchase steps on 'close' button press
	function hide_overlay() {
		$('.js-modal-overlay').addClass( "hidden" );

		// Enable scroll after overlay has been hidden
		$('body').removeClass('no-scroll');
		$('allrecords').removeClass('no-scroll'); // Scroll fix for mobile browsers
	}
  
  
  get_events();
  
  $("#input_phone").mask("+7 (999) 999-99-99");
  
  $("#contacts").on('submit', '#contacts_form', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: host + "orders/",
        data: {
            name: $("#input_name").val(),
            last_name: $("#input_last_name").val(),
            phone: $("#input_phone").val(),
            email: $("#input_email").val(),
            p_cart: $.cookie("p_cart"),
            city: $.cookie('city_tickets')
        },
        dataType: 'json',
        success: function (data) {
          $('#pay_form').attr('src', data.url);
        }
    });
    $.removeCookie('p_cart');
    $("#to_payment2").click();
  });
  
  
  $(".bootstrap-iso").on('mouseup', '.no-u', function (event) {
    alert($.cookie('city_tickets'));
    city = $(this).data("city");
    get_events(city);
    hide_overlay();
  });
  
  
  $("#bootstrap-iso").on('click', '#result', function (event) {
    $.ajax({
        type: 'POST',
        url: host + "orders/",
        data: {
            name: $("#input_name").val(),
            last_name: $("#input_last_name").val(),
            phone: $("#input_phone").val(),
            email: $("#input_email").val(),
            p_cart: $.cookie("p_cart"),
            city: $.cookie('city_tickets')
        },
        dataType: 'json',
        success: function (data) {

        }
    });
    $.removeCookie('p_cart');
    get_events();
    hide_overlay();
	});
  
  $("#bootstrap-iso").on('click', '.js-add-tickets', function (event) {
		event.preventDefault();
    evt_block = $(this).parents(".party-wrapper");
    $(this).parent( ".participate" ).hide();
    $(this).parent( ".participate" ).siblings(".tickets").removeClass('hidden');
    set_cart( evt_block.data('event-id'), evt_block.find('span.quant').text() );
	});
  
  $("#bootstrap-iso").on('click', '.js-add-cert', function (event) {
		event.preventDefault();
    cert_block = $("#cert-block");
    $(this).parent( ".participate" ).hide();
    $(this).parent( ".participate" ).siblings(".cert").removeClass('hidden').show();
    set_cart_cert( cert_block.data('cert-id'), cert_block.find('span.quant').text() );
	});

  
  $(document).on('click', '.tickets .tickets-counter__inc', function (e) {
    e.preventDefault();
    evt_block = $(this).parents(".party-wrapper");
    block = $(this).parents(".tickets");
    val_field = $(this).siblings('.tickets-counter__val');
    info_quant = block.find('span.quant');
    info_price = block.find('span.price');
    val = parseInt(val_field.text());
    cur_val = parseInt(val, 10) + 1;

    $.ajax({
      type: 'POST',
      url: host + "events/get_remain",
      data: {
          event: evt_block.data('event-id')
      },
      dataType: 'json',
      success: function (data) {
        remain = data.remain;
        if (remain >= cur_val)
        {
          val_field.text(hum_quant(cur_val));
          info_quant.text(hum_quant(cur_val));
          set_cart( evt_block.data('event-id'), cur_val );
        };  
      }  
    });
    
  });
  
  $(document).on('click', '.tickets .tickets-counter__dec', function (e) {
    e.preventDefault();
    evt_block = $(this).parents(".party-wrapper");
    block = $(this).parents(".tickets");
    val_field = $(this).siblings('.tickets-counter__val');
    info_quant = block.find('span.quant');
    info_price = block.find('span.price');

    val = parseInt(val_field.text());
    cur_val = parseInt(val, 10) - 1;
    if ( cur_val > 0 ) 
      {
        val_field.text(hum_quant(cur_val));
        info_quant.text(hum_quant(cur_val));
      }
    if (val === 1) {
      block.siblings( ".participate" ).removeClass('hidden').show();
      block.addClass('hidden');
    };
    set_cart( evt_block.data('event-id'), cur_val );
  });
  
  //CERTS
  $(document).on('click', '#cert-block .tickets-counter__inc', function (e) {
    e.preventDefault();
    block = $(this).parents("#cert-block");
    val_field = $(this).siblings('.tickets-counter__val');
    info_quant = block.find('span.quant');
    info_price = block.find('span.price');

    val = parseInt(val_field.text());
    cur_val = parseInt(val, 10) + 1;
    val_field.text(hum_quant(cur_val));
    info_quant.text(hum_quant(cur_val));
    set_cart_cert( block.data('cert-id'), cur_val );
 
  });
  
  $(document).on('click', '#cert-block .tickets-counter__dec', function (e) {
    e.preventDefault();
    block = $(this).parents("#cert-block");
    val_field = $(this).siblings('.tickets-counter__val');
    info_quant = block.find('span.quant');
    info_price = block.find('span.price');

    val = parseInt(val_field.text());
    cur_val = parseInt(val, 10) - 1;
    if ( cur_val > 0 ) 
      {
        val_field.text(hum_quant(cur_val));
        info_quant.text(hum_quant(cur_val));
      }
    if (val === 1) {
      block.find( ".participate" ).removeClass('hidden').show();
      block.find( ".cert" ).addClass('hidden').hide();
    };
    set_cart_cert( block.data('cert-id'), cur_val );
  });
  
  
  
  $(document).on('submit', '#new_order', function (e) {
    e.preventDefault();
    $.post(host + "orders/", $(this).serialize());
  });
  
  $(document).on('submit', '#promocode', function (e) {
    e.preventDefault();
    $.post(host + "orders/promo", $(this).serialize());
  });


  
	$('.js-modal-close, .js-back-to-parties-link').click(function() { 
		hide_overlay();
	});


	// Close overlay on overlay click (excluding overlay content)
	$('.modal-overlay').click(function(event) { 
    if( $(event.target).parents('.js-modal-overlay').length === 0 ) {
			hide_overlay();
		}
	});

	// Open overlay with purchase steps on 'purchase' button press
  $("#bootstrap-iso").on('click', '.js-open-cart', function (event) {
		event.preventDefault();
    $.ajax({
      type: "GET",
      url: host + "events/cart?cart=" + $.cookie("p_cart"),
      dataType: "script"
    });

		$('.js-modal-overlay').removeClass( "hidden" );
		$('.js-open-tickets-tab').tab("show");

		// Prevent backround scroll while overlay
		$('body').addClass('no-scroll');
    $('#nav5709394').css("visibility", "hidden");
		$('allrecords').addClass('no-scroll'); // Scroll fix for mobile browsers
	});


	// Activate ticket number widget
	$("input[name='tickets-number']").TouchSpin({
        min: 1,
        max: 10,
        initval: "1",
        step: 1,
        boostat: 5,
        maxboostedstep: 10,
        buttondown_class: "btn btn-default btn-painty-incrementor",
        buttonup_class: "btn btn-default btn-painty-incrementor"
    });

    // Show/hide promocode input field on promocode link click
    $("#bootstrap-iso").on('click', '.js-promocode', function (event) {
    	event.preventDefault();
    	$(".js-promocode-field").toggle();
    });


    // Synchronize promocode across two inputs
    $(".js-promocode-input-lg").keyup(function(){
    	$(".js-promocode-input-sm").val( $(".js-promocode-input-lg").val() );
    });
    $(".js-promocode-input-sm").keyup(function(){
    	$(".js-promocode-input-lg").val( $(".js-promocode-input-sm").val() );
    });

    // Apply promocode
  $(document).on('click', '.js-set-promocode', function (e) {
    e.preventDefault();
    //$(".js-promocode-text").text( $(".js-promocode-input").val() );
    $.ajax({
        type: 'POST',
        method: 'POST',
        url: host + "/events/promo",
        data: {
            promo:  $(".js-promocode-input").val(),
            p_cart: $.cookie("p_cart")
        },
        
        success: function (result) { eval(result) }
    });
  });
  
});  




