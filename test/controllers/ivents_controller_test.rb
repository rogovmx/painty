require 'test_helper'

class IventsControllerTest < ActionController::TestCase
  setup do
    @ivent = ivents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ivents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ivent" do
    assert_difference('Ivent.count') do
      post :create, ivent: { address: @ivent.address, admin_user_id: @ivent.admin_user_id, descr: @ivent.descr, end_date: @ivent.end_date, image: @ivent.image, max_guests: @ivent.max_guests, start_date: @ivent.start_date, title: @ivent.title, visible: @ivent.visible }
    end

    assert_redirected_to ivent_path(assigns(:ivent))
  end

  test "should show ivent" do
    get :show, id: @ivent
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ivent
    assert_response :success
  end

  test "should update ivent" do
    patch :update, id: @ivent, ivent: { address: @ivent.address, admin_user_id: @ivent.admin_user_id, descr: @ivent.descr, end_date: @ivent.end_date, image: @ivent.image, max_guests: @ivent.max_guests, start_date: @ivent.start_date, title: @ivent.title, visible: @ivent.visible }
    assert_redirected_to ivent_path(assigns(:ivent))
  end

  test "should destroy ivent" do
    assert_difference('Ivent.count', -1) do
      delete :destroy, id: @ivent
    end

    assert_redirected_to ivents_path
  end
end
