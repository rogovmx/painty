ActiveAdmin.register Order do
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters

  permit_params :client_id, :status, :promocode_id, order_items_attributes: [:id, :quant, :event_id, :_destroy]#, event_ids:[]
  
  config.sort_order = 'created_at_desc'
   
  menu priority: 5, :if => proc{ current_admin_user.admin? || current_admin_user.boss? }
  
  
  controller do 
    
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end
    
    def new
      @order = Order.new
      @order.order_items.build
    end
  end
  
  
  remove_filter :order_items

  index do
    column :created_at
    column :id
    column :updated_at do |order|
      order.updated_at if order.updated_at != order.created_at
    end
    column :status
    column :order_items do |order|
      ul do
        order.order_items.each do |oi| 
          li do
            link_to "#{oi.event.title} - #{oi.quant} шт.", painad_event_path(oi.event) if oi.event
          end
        end  
        if order.certificate
          li do
            link_to "#{order.certificate.cert_type.name} - #{order.certificate.quant} шт.", painad_certificate_path(order.certificate)
          end          
        end
      end
    end
    column :client_fi
    column :promo_name_p
    column :total_cost
    actions
  end
  
  
  show do
    attributes_table do
      row :id
      row :client_fi
      row :status
      row :promocode
      row :created_at
      row :updated_at
      row :total_cost
      row :order_items do
        ul do
          resource.order_items.each do |oi|
            li do
              link_to "#{oi.event.title} - #{oi.quant} шт.", painad_event_path(oi.event)
            end
          end
          if resource.certificate
            li do
              link_to "Сертификат: #{resource.certificate.cert_type.name} - #{resource.certificate.quant} шт.", painad_certificate_path(resource.certificate)
            end          
          end
        end  
      end
    end
  end
  
  
  

  form do |f|
    f.actions
    f.inputs "Заказ" do
      f.input :status
      f.input :client, as: :select, collection: Client.all.map { |c| [c.fie, c.id] }
      f.input :promocode
      f.has_many :order_items, allow_destroy: true do |oi|
        oi.input :event, as: :select, collection: Event.current.pluck(:title, :id)
        oi.input :quant 
      end
      #f.input :events
    end  
  end
  
  

end
