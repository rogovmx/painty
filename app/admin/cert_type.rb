ActiveAdmin.register CertType do
  permit_params :price, :display, :name, :category_id, :img
  
  menu priority: 5, :if => proc{ current_admin_user.admin? || current_admin_user.boss? }
  
  
  controller do 
    
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end

  end
  
  index do
    column :display
    column :name
    column :category
    column :price
    actions 
  end 
  
  show do
    attributes_table do
      row :display do
        resource.display ? status_tag("Yes") : status_tag("No")
      end
      row :name
      row :category
      row :price
      row :img do |c|
        image_tag c.img.url(:admin)
      end
    end
  end
  
  form do |f|
    #Debugging
    if f.object.errors.size >= 1
      f.inputs "Ошибки" do
        f.object.errors.full_messages.join('|')
      end
    end
    
    f.inputs "Сертификат" do
      f.input :display
      f.input :name
      f.input :category
      f.input :price
      f.input :img, hint: image_tag(f.object.img.url(:admin))

    end
    f.actions
  end
  
  
end
