ActiveAdmin.register Address do
  permit_params :name, :address, :url, :additional_name, :maps
  
  menu parent: 'Справочники', priority: 3, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
  
  controller do 
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end
  end
  
  
  index do
    column :name
    column :address
    column :url
    column :additional_name
    column :maps
    column :created_at
    column :updated_at

    actions
  end  
  
  
  
end
