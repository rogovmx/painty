ActiveAdmin.register Client do
  permit_params :name, :last_name, :phone, :email
   
   
  menu priority: 3, :if => proc{ current_admin_user.admin? || current_admin_user.boss? }
  
    
  controller do
    
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end
    
  end
 
  filter :email 
  filter :phone  
  filter :name  
  filter :last_name  
  filter :orders, as: :select, collection: Order.pluck(:id, :id)
  filter :created_at
  filter :updated_at
  
  index do
    column :email
    column :phone
    column :name
    column :last_name
    actions
  end  

  form do |f|
    columns do
      column do
        f.inputs 'ФИО' do
          f.input :name
          f.input :last_name
        end
      end
      column do
        f.inputs 'Контакты' do
          f.input :email
          f.input :phone, as: :email
        end
      end  
    end  
    f.actions
  end  
  

end
