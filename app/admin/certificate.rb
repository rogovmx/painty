ActiveAdmin.register Certificate do
  menu false
  permit_params :code, :discount, :buy_date, :paid, :client_id, :name

end
