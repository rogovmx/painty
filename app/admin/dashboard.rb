ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }
  content :title => proc{ I18n.t("active_admin.dashboard") } do
    columns do

#      column do
#        panel "Заказы" do
#          table_for Order.order('id desc').each do |order|
#            column("Id")   {|order| order.id }
##            column("Заказчик"){|order| link_to(order.client.email, painad_client_path(order.client)) }
#          end
#        end
#      end

      column do
        panel "Оплата" do
          if current_admin_user.painter? || current_admin_user.photographer?
            table_for Event.order('start_date desc').find_all { |evt| evt.admin_users.include?(current_admin_user) }.each do |event|
              column("Дата") { |event| event.start_date }
              column("Вечеринка") { |event| event.title }
              column("Ведущий") { current_admin_user.name }
              column("базовая З.П.") { current_admin_user.salary }
              column("Ставка") { current_admin_user.rate }
              column("З.П.") { current_admin_user.total_salary }
            end
          else
            table_for Event.order('start_date desc').each do |event|
              column("Дата") { |event| event.start_date }
              column("Вечеринка") { |event| event.title }
              column("Ведущие") { |event| event.admin_users.pluck(:name).join(', ') }
              column("З.П. ведущим") { |event| event.masters_salary }
            end
            
          end
          
          
        end
      end

    end # columns
  end  
end
