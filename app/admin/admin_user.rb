ActiveAdmin.register AdminUser do
  
  
  menu parent: 'Справочники', priority: 1, :if => proc{ current_admin_user.admin? }

    
  permit_params :email, :password, :password_confirmation, :name, :phone, :salary, :img, :rate, role_ids: []
  
  controller do
    def update
      if params[:admin_user][:password].blank?
        params[:admin_user].delete "password"
        params[:admin_user].delete "password_confirmation"
      end
      super
    end
  end
  
  index do
    selectable_column
    id_column
    column :name
    column :phone
    column :email
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :name
  filter :phone
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    #Debugging
    if f.object.errors.size >= 1
      f.inputs "Ошибки" do
        f.object.errors.full_messages.join('|')
      end
    end
    
    f.inputs "Пользователь подробно" do
      f.input :img, hint: image_tag(f.object.img.url(:w32))
      f.input :name
      f.input :phone
      f.input :email, hint: "Логин"
      f.input :password, hint: "Не вводите, если не требуется изменить" , required: false
      f.input :password_confirmation
      f.input :salary
      f.input :rate
      f.input :roles, as: :check_boxes
    end
    f.actions
  end
  
  show do
    attributes_table do
      row :img do |user|
        image_tag user.img.url(:w100)
      end
      row :name
      row :phone
      row :email
      row :salary
      row :rate
    end
    table_for admin_user.roles do
      column "Role" do |role|
        role.name
      end
    end

  end
  

end
