ActiveAdmin.register Category do
  permit_params :name

 
  menu parent: 'Справочники', priority: 2, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
   
  controller do
    
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end
    
  end

  index do
    column :name
    column :created_at
    column :updated_at

    actions
  end  

end
