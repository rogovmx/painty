require 'trigger'
ActiveAdmin.register Event do
  permit_params :title, :descr, :start_date, :end_date, :image, :address_id, 
    :max_guests, :visible, :category_id, :price, :discount, admin_user_ids:[]

   
  menu priority: 3 #, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
 
  scope "Действующие вечеринки", :current, default: true#, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
  scope "Все вечеринки", :all#, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
  
  controller do
    
    def scoped_collection
      if current_admin_user.painter? || current_admin_user.photographer?
        super.all_for_user current_admin_user.id
      else
        super
      end  
    end
    
    def action_methods
      if current_admin_user.painter?
        super - ["edit", 'destroy']
      elsif current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end

  end
  
#  actions :all, except:[ :new, :destroy, :edit ], :if => proc{ current_admin_user.painter? }
  
  filter :visible  
  filter :discount  
  filter :title  
  filter :descr  
  filter :category  
  filter :address, as: :select, collection: Address.pluck(:address, :id)
  filter :price  
  filter :start_date  
  filter :end_date  
  filter :admin_users, as: :select, collection: AdminUser.pluck(:name, :id)
  
  
  member_action :clone, method: :get do
    copy = Event.create! do |c|
      c.title = resource.title + " КОПИЯ"
      c.descr = resource.descr
      c.discount = resource.discount
      c.max_guests = resource.max_guests
      c.visible = resource.visible
      c.category_id = resource.category_id
      c.price = resource.price
      c.address_id = resource.address_id
      c.image = resource.image
      c.start_date = resource.start_date
      c.end_date = resource.end_date
    end

    redirect_to edit_painad_event_path(copy.id), notice: "Вечеринка \"#{resource.title}\" скопировано в \"#{copy.title}\""
  end  
  
  action_item :clone, only: :show do
    link_to "Копия", clone_painad_event_path, data: {confirm: 'Копировать данную вечеринку?'} , method: :get unless current_admin_user.painter?
  end

  
  
  index do
    column :visible
    column :discount do |evt|
      evt.discount ? status_tag("Yes") : status_tag("No")
    end
    column :category
    column :title
    column :start_date
    column :end_date
    column :for_admin? do |evt|
      evt.for_admin? ? status_tag("Yes") : status_tag("No")
    end
    column :address do |a|
      link_to [a.address.name, a.address.address].compact.join(", "), painad_address_path(a.address.id) if a.address
    end
    column :max_guests
    column :remain
    column :price
    column :admin_users do |event|
      event.admin_users.pluck(:name).join(", ") if event.admin_users
    end
    actions 
  end  
  
  show do
    attributes_table do
      row :visible do
        resource.visible ? status_tag("Yes") : status_tag("No")
      end
      row :discount do 
        resource.discount ? status_tag("Yes") : status_tag("No")
      end
      row :category
      row :title
      row :descr
      row :start_date
      row :end_date
      row :admin_link_url do |i|
        link_to i.admin_link_url, i.admin_link_url, target: "_blank" if i.admin_link_url
      end
      row :address do |a|
        link_to [a.address.name, a.address.address].compact.join(", "), painad_address_path(a.address.id) if a.address
      end
      row :max_guests
      row :remain
      row :price    
      row :cost_price 
      row :admin_users do 
        resource.admin_users.pluck(:name).join(", ") if resource.admin_users
      end
    end
  end
  
  form html: {multipart: true} do |f|
    columns do
      column do
        f.inputs "Редактирование вечеринки" do
          f.input :visible
          #f.input :discount
          f.input :category
          f.input :title
          f.input :descr, input_html: { rows: 2 }
          f.input :address, as: :select, collection: Address.pluck(:name, :address, :id).map { |a| [a[0].to_s + ", " + a[1].to_s, a[2].to_s] }
          f.input :start_date, as: :date_time_picker, datepicker_options: { min_date: Time.now.to_date }
          f.input :end_date, as: :date_time_picker, datepicker_options: { min_date: Time.now.to_date }
          f.input :admin_users, as: :select, include_blank: true, collection: AdminUser.pluck(:name, :id)#, selected: @resource.admin_user ? @resource.admin_user.id : nil
          f.input :max_guests
          f.input :price
        end
      end  
      column do
        f.inputs "Редактирование банера" do
          f.input :image, hint: image_tag(f.object.image.url(:w200))
        end
      end
    end
    f.actions
  end  

  
end
