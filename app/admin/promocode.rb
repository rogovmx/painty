ActiveAdmin.register Promocode do
  permit_params :name, :discount, :start_date, :end_date, :active, :event_id

  menu priority: 5, :if => proc{ current_admin_user.admin? || current_admin_user.boss? || current_admin_user.manager? }
 
    
  controller do
    
    def action_methods
      if current_admin_user.painter? || current_admin_user.photographer?
        super - ["edit", 'destroy', 'show']
      else
        super 
      end
    end
    
  end
  
  index do 
    column :active
    column :name
    column :number_of_uses
    column :discount
    column :start_date
    column :end_date
    #column :event
    actions
  end    

  form do |f|
    f.inputs do
      f.input :active
      f.input :name
      f.input :discount
      f.input :start_date, as: :datepicker
      f.input :end_date, as: :datepicker
      #f.input :event
    end
    f.actions
  end
  

end
