module Discounts
  extend ActiveSupport::Concern

  def hum_quant
    pl =
      if (quant == 1)
        " место " 
      elsif quant > 1 && quant < 5 
        " места "
      elsif quant > 4
        " мест "
      end  
    quant.to_s + pl
  end

  def percent_by_quant
#    return 0 unless event.discount
    if (3..4).include? quant.to_i  
      10
    elsif quant.to_i > 4
      15
    else
      0
    end
  end

end

