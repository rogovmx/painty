class Promocode < ActiveRecord::Base
  belongs_to :event
  has_many :order_items
  has_many :orders
  has_many :carts

  audited
  resourcify
  
  validates :name, uniqueness: true, presence: true
  validates :discount, presence: true  
  
  before_save :fix_discount
  
  scope :current, ->{ where("(start_date IS NULL OR start_date < ?) or (end_date IS NULL OR end_date > ?) AND active = ?", Time.now, Time.now, true) }
  
  
  def number_of_uses
    orders.size
  end
  
  private
  
  def fix_discount
    self.discount = discount.to_d
  end
  
end
