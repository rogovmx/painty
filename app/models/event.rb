class Event < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  has_many :order_items
  has_many :orders, through: :order_items
  has_many :clients, through: :orders
  has_and_belongs_to_many :admin_users
  belongs_to :category
  belongs_to :address
  has_many :promocodes
  has_many :cart_items
  
  audited
  resourcify
  
  before_save :create_admin_link
  
  has_attached_file :image,
                  styles: { admin: '255x100#', w200: '250', w360: '360' }
  validates_attachment_content_type :image,
                                    content_type: /\Aimage\/.*\Z/
                
  scope :all_for_user, ->(id) { joins("INNER JOIN admin_users_events ON admin_users_events.event_id = events.id AND admin_users_events.admin_user_id = #{id}")}
  scope :visible, ->{ where visible: true }
  scope :current, ->{ visible.where("start_date > ?", Time.now - 30.minutes).order("events.start_date ASC") }
  scope :current_for_user, ->(id) { current.where admin_user_id: id }
#  scope :for_admins, ->{ visible.where("start_date > ? AND start_date < ?", Time.now - 30.minutes, Time.now + 2.hours) }
  scope :for_admins, ->{ visible.where("(start_date > ?) AND (end_date > ?)", Time.now - 30.minutes, Time.now) }
  scope :filter_by_city, ->(city){ includes(:category).where(categories: {name: city == "SPB" ? "Санкт-Петербург" : "Москва"}).references(:category).order("events.start_date ASC") }
  
  
  validates :title, :start_date, :end_date, :image, :address_id, :max_guests, :category_id, presence: true
  
  def painter
    admin_users.includes(:roles).where(roles: { name: "Художник" }).references(:roles).first || AdminUser.new
  end
  
  def human_date
    if start_date < Date.tomorrow and start_date > Date.today
      'Сегодня'
    elsif (start_date + 1.day) < Date.tomorrow and start_date > Date.today
      'Завтра'
    elsif (start_date + 2.days) < Date.tomorrow and (start_date + 1.day) > Date.tomorrow
      'Послезавтра'
    elsif start_date.wday == 1
      "Понедельник"
    elsif start_date.wday == 2
      "Вторник"
    elsif start_date.wday == 3
      "Среда"
    elsif start_date.wday == 4
      "Четверг"
    elsif start_date.wday == 5
      "Пятница"
    elsif start_date.wday == 6
      "Суббота"
    elsif start_date.wday == 7
      "Воскресенье"
    end
  end
  
  def full_date
    date = I18n.l start_date, format: :long
    "#{human_date}, #{date}"
  end
   
  def short_date
    I18n.l start_date, format: :short
  end

  def email_date
    I18n.l start_date, format: '%-d %B %H:%M'
  end
  
  def current?
    visible == true && start_date > (Time.now - 30.minutes)
  end
  
  def in_cart? cart
    return false unless cart
    cart.cart_items.current.map { |c| c.event }.include?(self)
  end
  
  def cart_quant cart
    return 1 unless in_cart? cart
    cart.cart_items.current.find { |c| c.event == self }.quant
  end
  
  def cart_cost cart
    return price unless in_cart? cart
    cart.cart_items.current.find { |c| c.event == self }.discount_cost# { |sum, i| sum + i.discount_cost}
  end
  
  def cart_discount_perc cart
    return "0%" unless in_cart? cart
#    "#{cart.cart_items.current.find { |c| c.event == self }.percent_by_quant}%"
    "#{cart.percent_by_quant}%"
  end
  
  def remain
    q = order_items.includes(:order).where(orders: {status: 1}).references(:order).sum(:quant)
    max_guests - q.to_i
  end
  
  def promocode
    promocodes.current.where.not(start_date: nil).order("start_date desc").first || promocodes.current.first
  end
  
  def rebuild_admin_link
    self.update_attribute :admin_link, SecureRandom.uuid
  end
  
  def for_admin?
    visible && (start_date > Time.now - 30.minutes) && (end_date > Time.now)
  end
  
  def admin_link_url
    host = Rails.env.production? ? "http://painty.rogov.lclients.ru/" : "http://localhost:3001/"
    host + "events/#{admin_link}/tickets" if for_admin?
  end
  
  def masters_salary
    admin_users.map { |au| au.total_salary }.sum
  end
  
  def cost_price
    masters_salary / (order_items.payed.count > 0 ? order_items.payed.count : 1)
  end



  def price_format price
    number_to_currency(price,  separator: ",", delimiter: " ", precision: 0, unit: 'р. ', format: "%n %u")
  end
  
  private
  
  def create_admin_link
    self.discount = true
    self.admin_link = SecureRandom.uuid unless self.admin_link
  end
  
 
end
