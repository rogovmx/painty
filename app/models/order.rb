class Order < ActiveRecord::Base
  include Discounts
  include ActionView::Helpers::NumberHelper
  
  has_many :order_items
  has_associated_audits
  has_many :events, through: :order_items
  belongs_to :client
  belongs_to :promocode
  belongs_to :certificate
  
  # TODO:  поставить :send_creation_mail после оплаты
  # after_create :send_creation_mail


  resourcify
  
  
  scope :payed, -> { where(status: 1) }
  
  validates :client_id, presence: true
  
#  accepts_nested_attributes_for :events
  accepts_nested_attributes_for :order_items, allow_destroy: true
  
  before_save :randomize_id
  #after_save :retail_order
  
  def quant
    q = order_items.inject(0) { |sum, i| sum + i.quant }
    q += certificate.quant if certificate
    q
  end

  
  def cost
    c = order_items.inject(0) { |sum, i| sum + i.cost } 
    c += certificate.cost if certificate
    c
  end
  
  def total_cost
    t_cost = cost
    if promocode
      t_cost = cost - cost * promocode.discount.to_d / 100
    end
    t_cost
  end

  def price_format_total_cost
    number_to_currency(total_cost,  separator: ",", delimiter: " ", precision: 0, unit: 'р. ', format: "%n %u")
  end
  
  def client_fi
    "#{client.name} #{client.last_name} " if client
  end
  
  def promo_name_p
    "#{promocode.name} (#{promocode.discount.to_d}%)" if promocode
  end
  
  def payed?
    status == 1
  end

  def send_creation_mail
    Trigger.send_order self
  end
  
  private
  
  def retail_order
    reload
#    go! { 
        Retail.create_or_update_order self 
#        }
  end
  
  def randomize_id
    begin
      self.id = rand(10_000_000..99_999_999)
    end while Order.where(id: self.id).exists?
  end
end
