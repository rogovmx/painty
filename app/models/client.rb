class Client < ActiveRecord::Base
  require 'retail'
  
  has_many :orders
  has_many :events, through: :orders
  
  audited
  resourcify
  
  after_save :create_or_update_retail_customer
  
#  validates :name, presence: true
  
  def fie
    "#{name} #{last_name} #{email}"
  end
  
  private
  
  
  def create_or_update_retail_customer
    go! {Retail.create_or_update_customer self}
  end
  
end
