class Certificate < ActiveRecord::Base

  belongs_to :cert_type
  
  def cost
    quant * cert_type.price
  end
  
  
  # TODO: после оплаты (наверное) :send_creation_mail
  # after_create :send_creation_mail

  def send_creation_mail
    Trigger.send_certificate self
  end

end
