class Address < ActiveRecord::Base
  validates :address, presence: true, allow_nil: true
  has_many :events
  audited
  resourcify
  
  default_scope { order :name }
  
end
