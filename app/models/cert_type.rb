class CertType < ActiveRecord::Base
  has_many :certificates
  belongs_to :category
  
  has_attached_file :img,
                  styles: { admin: '255x100#', w200: '200', w360: '360' }
  validates_attachment_content_type :img,
                                    content_type: /\Aimage\/.*\Z/
  
  validates :name, :category_id, :price, :img, presence: true
  
  scope :displayed, -> { where display: true }
  scope :filter_by_city, ->(city){ includes(:category).where(categories: {name: city == "SPB" ? "Санкт-Петербург" : "Москва"}).references(:category).limit 1 }
  
end
