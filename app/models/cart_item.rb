class CartItem < ActiveRecord::Base
  include Discounts
  
  belongs_to :cart
  belongs_to :event
  
  scope :current, -> { all.find_all { |c| c.event && c.event.current? }.sort_by { |c| c.event.start_date } }

  def valid_event?
    event && event.current?
  end
  
  def cost
    (quant || 0) * event.price
  end
  
  def discount_cost
    return 0 unless event.discount
    sum = (quant || 0) * event.price
    (sum - sum.to_f / 100.0 * cart.percent_by_quant.to_f).to_i
  end
  
  def discount_rub
    return 0 unless event.discount
    sum = (quant || 0) * event.price
    sum.to_f / 100.0 * cart.percent_by_quant.to_f   
  end
  
end
