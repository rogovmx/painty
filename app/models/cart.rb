class Cart < ActiveRecord::Base
  include Discounts
  
  has_many :cart_items, dependent: :destroy
  belongs_to :promocode
  belongs_to :certificate
  
  def total_cost
    t_cost = cost
    t_cost -= cost.to_f / 100.0 * percent_by_quant.to_f
    if promocode
      t_cost = t_cost - t_cost * promocode.discount.to_d / 100
    end
    t_cost
  end
  
  def cost
    c = cart_items.current.inject(0) { |sum, i| sum + i.cost } 
    c += certificate.cost if certificate
    c
  end
  
  def total_quant
    q = cart_items.current.inject(0) { |sum, i| sum + i.quant }
    q += certificate.quant if certificate
    q
  end
  
  alias_method :quant, :total_quant
  
  def total_discount_rub
    cost - total_cost
  end

  
end
