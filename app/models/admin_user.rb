class AdminUser < ActiveRecord::Base
  self.primary_key = "id"
  rolify
  has_and_belongs_to_many :events
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable
  has_many :events     
  audited

  validates :name, :email, :salary, :rate, presence: true
  validates :email, uniqueness: true
  
  has_attached_file :img,
                styles: { w100: '100', w32: '32' }
  validates_attachment_content_type :img,
                      content_type: /\Aimage\/.*\Z/
  
  
  def admin?
    roles && roles.pluck(:name).include?("admin")
  end
  

  def boss?
    roles && roles.pluck(:name).include?("Босс")
  end
  

  def manager?
    roles && roles.pluck(:name).include?("Менеджер")
  end
  

  def painter?
    roles && roles.pluck(:name).include?("Художник")
  end

  def photographer?
    roles && roles.pluck(:name).include?("Фотограф")
  end
  
  def total_salary
    salary && rate ? salary * rate : 0
  end
  
  
#  http://activeadmin.info/docs/13-authorization-adapter.html#using_the_cancan_adapter
#https://github.com/RolifyCommunity/rolify/wiki
end
