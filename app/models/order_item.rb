class OrderItem < ActiveRecord::Base
  self.primary_key = :id
  
#  after_save :retail_order
  
  belongs_to :order
  audited associated_with: :order

  belongs_to :event
  belongs_to :promocode
  
  scope :used, ->{ where("activated >= quant") }
  scope :not_used, ->{ where("activated < quant") }
  scope :payed, -> { includes(:order).where( orders: { status: nil } ) }
  
  validates :event_id, :quant, presence: true
  
  def used?
    activated.to_i >= quant.to_i
  end
    
  def promo_percent
    order.promocode && event.promocodes.include?(order.promocode) ? order.promocode.discount : 0
  end
  
  def cost
    if event
      (quant || 0) * event.price
    else
      0
    end  
  end
  
  def discount_cost
    return 0 unless event
    sum = (quant || 0) * event.price
    sum - sum.to_f / 100.0 * percent_by_quant.to_f
  end

  def discount_value
    return 0 unless event
    sum = (quant || 0) * event.price
    sum.to_f / 100.0 * percent_by_quant.to_f
  end
  
  def percent_by_quant
    return 0 unless event.discount
    if (3..4).include? quant.to_i  
      10
    elsif quant.to_i > 4
      15
    else
      0
    end
  end
  
  private
  
  
end
