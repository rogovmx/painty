class Category < ActiveRecord::Base
  has_many :events
  has_many :cert_types
  validates :name, uniqueness: true, presence: true
  audited
  resourcify
end
