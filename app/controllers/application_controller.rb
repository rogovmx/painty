class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  protect_from_forgery with: :null_session
  before_action  :set_access_control_headers
  
  

  def current_user_or_admin
    current_admin_user || current_user || AdminUser.new
  end
  
  def set_access_control_headers
#    headers['Access-Control-Allow-Origin'] = 'http://localhost:3001'
    headers['Access-Control-Allow-Origin'] = '*'
  end
  
    
  def get_pay_status order
    req = "https://sandbox.payture.com/apim/Pay"
    resp = RestClient.get req
    success = Nokogiri::XML(resp).xpath("//PayStatus").first['Success']
  end
  
    
  def pay_online order
    key = "Key=MerchantPainty&"
#   IP=#{request.remote_ip};"
    data = "SessionType=Pay;OrderId=#{order.id};Amount=#{(order.total_cost*100).to_i}
        ;IP=78.140.203.154;Description=MyTestTransaction;Url=http://painty.ru?orderid=#{order.id}&result=success;
        Total=#{order.total_cost};Product=Painty"
    req = "https://sandbox.payture.com/apim/Init?" + key + "Data=" + CGI::escape(data)  
    resp = RestClient.get req
    pay_session = Nokogiri::XML(resp).xpath("//Init").first['SessionId']
    url = "https://sandbox.payture.com/apim/Pay?SessionId=#{pay_session}"
  end
  
end
