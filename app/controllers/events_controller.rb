class EventsController < ApplicationController
  include ActionView::Helpers::NumberHelper
  
  skip_before_filter :verify_authenticity_token
#  before_action :is_xhr?
  protect_from_forgery only: [:tickets, :activate, :deactivate]
  before_action  :img_url, :set_access_control_headers, except: [:tickets, :activate, :deactivate]

  helper_method :price_format, :hum_quant
  
  def index
    @cert = CertType.displayed.filter_by_city(params[:city]).first
    @events = Event.current.filter_by_city(params[:city])
    if @events.any?
      @first_evt = @events.first   
      @events = @events[1..-1]
    end  
    if params[:p_cart].present?
      @cart = Cart.find_by_id(params[:p_cart])
      if @cart
        @cart.cart_items.find_all { |ci| !ci.valid? }.each { |ci| ci.delete } 
        @cart.reload
      end  
    end 
  end

  def promo
    promo = Promocode.current.find_by_name(params[:promo])
    @cart = Cart.find_by_id(params[:p_cart])
    @cart_items = @cart.cart_items.current
    if @cart
      @cart.promocode = promo
      @cart.save
      @cart.reload     
    end
#    render text: promo.inspect
    render 'cart.js'
  end

  
  def edit_cart
    cart = Cart.find_by_id(params[:p_cart]) || Cart.create
    event = Event.current.find_by_id(params[:event])
    if event
      exist = cart.cart_items.where(event_id: event.id)
      if exist.any?
        if params[:quant].to_i < 1
          exist.delete_all
        else  
          exist.first.update_attribute :quant, params[:quant].to_i
        end
      else  
        cart.cart_items << CartItem.create(event_id: params[:event], quant: params[:quant].to_i )
      end
    end  
    cart.cart_items.find_all { |ci| !ci.valid_event? }.each { |ci| ci.delete }
    cart.reload
    render json: {p_cart: cart.id, discount: cart.percent_by_quant.to_s + "%", evts: cart.cart_items.current.map { |ci| [ci.event.id, ci.event.cart_cost(cart)] }}
  end
  
  def edit_cart_cert
    cart = Cart.find_by_id(params[:p_cart]) || Cart.create
    cert = CertType.find_by_id(params[:cert])
    if cert
      exist = cart.certificate if cart.certificate && cart.certificate.cert_type == cert
      if exist
        if params[:quant].to_i < 1
          exist.delete
        else  
          exist.update_attribute :quant, params[:quant].to_i
        end
      else  
        cart.certificate.delete if cart.certificate
        cart.certificate = Certificate.create(
          cert_type_id: cert.id, quant: params[:quant].to_i
        )
        cart.save
      end
    end  
    cart.reload
    render json: {p_cart: cart.id, cost: cart.certificate ? cart.certificate.cost : 0, discount: cart.percent_by_quant.to_s + "%", evts: cart.cart_items.current.map { |ci| [ci.event.id, ci.event.cart_cost(cart)] }}
  end
    
  def get_remain
    event = Event.current.find_by_id(params[:event])
    render json: {remain: event.remain}
  end
  
  
  
  def cart
    @cart = Cart.find_by_id params[:cart]
    @cart_items = @cart.cart_items.current
  end
  
  def tickets
    @event = Event.for_admins.find_by_admin_link params[:id]
    unless @event
      render text: "Вечеринка не найдена!"
    else
      @tickets = @event.order_items.sort_by { |oi| oi.order.client.fie }
      @page_title = "Painty. Билеты на вечеринку: #{@event.title}" 
      render layuot: 'application'
    end  
  end
  
  def activate
    @order_item = OrderItem.find params[:ticket]
    @order_item.update_attribute :activated, @order_item.activated.to_i + 1 if @order_item.activated < @order_item.quant
    @order_item.reload
  end
  
  def deactivate
    @order_item = OrderItem.find params[:ticket]
    @order_item.update_attribute :activated, @order_item.activated.to_i - 1 if @order_item.activated > 0
    @order_item.reload
    render :activate
  end
  
    
  def price_format price
    number_to_currency(price,  separator: ",", delimiter: " ", precision: 0, unit: 'р. ', format: "%n %u")
  end
  
  def hum_quant(quant)
    pl =
      if (quant == 1)
        " место " 
      elsif quant > 1 && quant < 5 
        " места "
      elsif quant > 4
        " мест "
      end  
    quant.to_s + pl
  end
  
  
  
  def set_access_control_headers
#    headers['Access-Control-Allow-Origin'] = 'http://localhost:3001'
    response.headers["Content-Type"] = "text/javascript; charset=utf-8"
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = '*, x-requested-with'
  end
  
  
  private
    
    def img_url
      @img_url = Rails.env.production? ? "http://painty.rogov.lclients.ru" : 'http://localhost:3001'
    end
  
    def is_xhr?
      render text: "No" unless request.xhr?
    end

    def event_params
      params.require(:event).permit(:title, :descr, :start_date, :end_date, :image, :address, :admin_user_id, :max_guests, :visible)
    end
end

