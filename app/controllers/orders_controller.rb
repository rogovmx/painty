class OrdersController < InheritedResources::Base

  def create
    cart = Cart.find_by_id params[:p_cart]
    client = Client.find_or_initialize_by(email: client_params[:email])
    client.without_auditing do
      client.save
      client.update_attributes(client_params)
    end
#    order = Order.create(client_id: client.id, promocode_id: order_params[:promocode_id])
    order = Order.create(client_id: client.id, promocode_id: cart.promocode_id, certificate_id: cart.certificate_id)
    cart.cart_items.current.each do |ci|
      order.order_items << OrderItem.new(event_id: ci.event_id, quant: ci.quant)
    end  
    cart.delete
#    go! { 
      Retail.create_or_update_order order 
#      }
#    pay_session = 
    render json: {url: pay_online(order)}
#    render js: "console.log('#{pay_session}');$('#show_event').html('');$('#show_event').hide();$('#black_fog').hide();window.open('https://sandbox.payture.com/apim/Pay?SessionId=#{pay_session}', '_blank')"
  end
  
  
  private
    
    def current_user
      current_admin_user || nil
    end

    def order_item_params
      params.require(:order_item).permit(:order_id, :event_id, :quant)
    end
    
    def client_params
      params.permit(:name, :last_name, :phone, :email)
    end
    
    def order_params
      params.require(:order).permit(:client_id, :promocode_id, :status)
    end
end

