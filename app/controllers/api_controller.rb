class ApiController < ApplicationController
  layout false
  
  def index
    render text: "Painty #{request.remote_ip}"
  end
  
  def retail_catalog
    @categories = Category.all
    @cert_types = CertType.displayed
    @events = Event.visible
    response.charset = "utf-8"
    respond_to do |format|
      format.xml
    end
  end
  
  def pay_status
    order = Order.find params[:orderid]
    order.update_attribute :status, 1 if order && pay_online(order) == "True"
    render nothing: true
  end

  
end
