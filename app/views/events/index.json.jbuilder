json.array!(@events) do |event|
  json.extract! event, :id, :title, :descr, :start_date, :end_date, :image, :address, :admin_user_id, :max_guests, :visible
  json.url event_url(event, format: :json)
end
